mod cli;
use clap::Parser;
use figlet_rs::FIGfont;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;

#[derive(Debug, Serialize, Deserialize)]
struct CfgInitScript {
    metadata: Meta,
    svc: Service,
}
#[derive(Debug, Serialize, Deserialize)]
struct Meta {
    name: String,
    description: String,
    before: Option<String>,
}
#[derive(Debug, Serialize, Deserialize)]
struct Service {
    exec: std::path::PathBuf,
    deps: RefCell<Vec<String>>,
    env: RefCell<Vec<String>>,
}
#[derive(Debug, Serialize, Deserialize)]
struct Cfg {
    initpath: std::path::PathBuf,
    boot_runlevel: std::path::PathBuf,
    initscriptpath: std::path::PathBuf,
}

fn start_stop_svc(action: bool) -> anyhow::Result<(), anyhow::Error> {
    if action == true {
        let font = FIGfont::standard().unwrap();
        let cfg = toml::from_str::<Cfg>(&std::fs::read_to_string("/etc/init.toml")?)?;
        let path = cfg.initscriptpath;
        for _i in std::fs::read_dir(path).iter() {}
        font.convert("Runlevel mode");
    } else if action == false {
        println!("Not running as PID1");
    };
    Ok(())
}
fn main() -> anyhow::Result<(), anyhow::Error> {
    let _a = crate::cli::Args::parse();
    let path = std::path::Path::new("/etc/")
        .join("init.toml")
        .to_path_buf();
    if let Some(_a) = _a.create.as_ref() {
        println!(">> Creating new runlevel");
    }
    if let Some(_b) = _a.mkconfig.as_ref() {
        println!(">> Creating new init configuration file at /etc/init.toml");
        std::fs::File::create(path)?;
        let conf = Cfg {
            initpath: "somewhere".to_string().into(),
            boot_runlevel: "default".to_string().into(),
            initscriptpath: "/mtos/init/scripts".to_string().into(),
        };
        std::fs::File::create("/etc/init.toml")?;
        std::fs::write("/etc/init.toml", toml::to_string(&conf)?)?;
    }
    if let Some(_mk) = _a.mkconfig.as_ref() {
        let curdir = std::env::current_dir()?;
        println!(
            ">> Creating new service at {:?}",
            std::path::Path::new(&curdir.to_path_buf())
        );
        std::fs::File::create(std::path::Path::new(&curdir.to_path_buf()).join("service.toml"))?;
        let conf = CfgInitScript {
            metadata: Meta {
                name: "".to_string(),
                description: "".to_string(),
                before: Some("".to_string()),
            },
            svc: Service {
                exec: std::path::Path::new("").to_path_buf(),
                deps: [String::from("")].to_vec().into(),
                env: [String::from("")].to_vec().into(),
            },
        };
        std::fs::write(std::path::Path::new(&curdir.to_path_buf()).join("service.toml"), toml::to_string(&conf)?)?;
        println!(">> DONE\n>> Make sure you edit this file before using it.");
    }
    if let Some(c) = _a.pid1.as_ref() {
        if c == &true {
            start_stop_svc(true)?;
        } else if c == &false {
            start_stop_svc(false)?;
        }
    }
    Ok(())
}
