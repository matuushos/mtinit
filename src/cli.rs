use clap::Parser;
#[derive(Debug, Parser)]
#[command(
    author,
    version,
    about,
    long_about = "PID 1 (Init system) for MatuushOS"
    )]
pub struct Args {
    /// Enable service at boot
    #[arg(long)]
    pub(crate) enable: Option<String>,
    /// Disable service
    #[arg(long)]
    pub(crate) disable: Option<String>,
    #[arg(long)]
    /// Create runlevel
    pub(crate) create: Option<String>,
    #[arg(long)]
    /// Delete runlevel
    pub(crate) delete: Option<String>,
    /// Switch to runlevel
    #[arg(long)]
    pub(crate) runlevel: Option<String>,
    /// Create service configuration
    #[arg(long)]
    pub(crate) mkservice: Option<String>,
    /// Create MtInit configuration
    #[arg(long)]
    pub(crate) mkconfig: Option<bool>,
    /// Start system as PID1
    #[arg(long)]
    pub(crate) pid1: Option<bool>,
}
